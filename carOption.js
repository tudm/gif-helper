/* eslint-disable no-await-in-loop */
/* eslint-disable prefer-destructuring */
const url = "https://www3.nissan.co.jp/vehicles/new/leaf/simulation.html#configure/BABT8/ASFUfYHQBGwTUuw/version";
// const url = "https://www3.nissan.co.jp/vehicles/new/kicks/simulation.html#configure/BABUq/A0_SxLCsP0/version";

const carInfo = url.split("#configure/")[1].split("/version")[0].split("/").join(":");
console.log(`get car's option from ${url}`);

const axios = require("axios");
const cheerio = require("cheerio");
const _ = require("lodash");
const moment = require("moment");
const fs = require("fs");

const getListCar = async () => {
    let carName = "";
    let res = await axios.get(url);
    let $ = cheerio.load(res.data);
    let apiKey;
    let clientKey;

    const scripts = $("script");
    for (let i = 0; i < scripts.length; i++) {
        const script = $(scripts[i]).html();
        if (script.indexOf("HELIOS.components.c_cfg_vuejs.dataPaths") > -1) {
            let strObj = script.split("HELIOS.components.c_cfg_vuejs.dataPaths =")[1];
            strObj = strObj.substr(strObj.indexOf("{"), strObj.indexOf("}"));
            carName = JSON.parse(strObj).rootPath.split("/")[7];
        } else if (script.indexOf('"apiKey":') > -1) {
            let strObj = script.split(" var HELIOS = ")[1].split("googleDataLayer = [HELIOS.")[0].trim();
            strObj = strObj.substr(0, strObj.length - 1);
            let obj = JSON.parse(strObj.replace(/\n/g, "").replace(/'/g, '"').replace('"storageExpiryPeriod": "",', '"storageExpiryPeriod": ""'));
            apiKey = _.get(obj, "config.common.api.apigee.apiKey");
            clientKey = _.get(obj, "config.common.api.apigee.clientKey");
        }
    }
    carName = carName || url.split("/")[5];

    console.log("carName", carName);
    console.log("...getting car list");

    const jsonUrl = `https://www3.nissan.co.jp/content/nissan_prod/ja_JP/index/vehicles/new/${carName}/simulation/jcr:content/core.version.versions.properties.key.name.exteriorColours.engine.usp.universalPriceDescription.universalPriceDisclaimer.gradeName.images.versionUspSubheading.stylePacks.techSpecs.fullTechnicalSpecs.priceList.json`;

    res = await axios.get(jsonUrl);
    const carList = _.get(res.data, "versions");
    const carListOptions = [];

    // carList.length = 1;

    let urlCarLink = `https://ap.nissan-api.net//v2/models/8E_2/configuration/${carInfo}/choices?useTransitionalData=false&choicePriceType=Retail+with+VAT&priceMethod=version&priceType=Retail+with+VAT&filterByChoiceIDs=${carList
        .map((c) => c.key)
        .join(",")}&conflictTypes=USER,PRICE`;

    res = await axios.get(urlCarLink, {
        headers: {
            apiKey,
            clientKey,
        },
    });

    let carUrlMap = {};
    _.forEach(_.get(res.data, "choices"), (x) => {
        carUrlMap[x.choiceId] = `https://www3.nissan.co.jp/vehicles/new/${carName}/simulation.html#configure/${x.configurationURI
            .split("/")
            .pop()
            .split(":")
            .join("/")}/individual-options`;
    });

    console.log("Found", carList.length, "car");
    for (let i = 0; i < carList.length; i++) {
        const car = carList[i];
        const carUrl = `https://www3.nissan.co.jp/content/nissan_prod/ja_JP/index/vehicles/new/${carName}/simulation/jcr:content/core.version.versions.${car.key}.properties.json`;
        console.log("Checking", i + 1, carName, car.key);
        res = await axios.get(carUrl);

        const fullEquipmentSpecs = _.get(res.data, "versions[0].fullEquipmentSpecs")
            .filter((x) => x.key && x.specs[0].showToolTip == false && x.specs[0].hidden == false)
            .map((x) => {
                x.specs = x.specs.filter((y) => y.modelType == "OPTION");
                x.desc = x.specs.map((y) => y.value).join("\n");

                return x;
            });

        const accessories = _.get(res.data, "versions[0].accessories").map((x) => {
            x.desc = x.specs.map((y) => y.value).join("\n");

            return x;
        });

        carListOptions.push({
            carKey: car.key,
            url: carUrlMap[car.key],
            carName: car.name,
            gradeName: car.gradeName,
            price: `VAT Only: ${car.priceList["VAT Only"]} , Retail with VAT: ${car.priceList["Retail with VAT"]}`,
            fullEquipmentSpecs: _(fullEquipmentSpecs)
                .filter((o) => o.desc)
                .map((o, idx) => `=== ${idx + 1}. ${o.value}==============================\n\n${o.desc}\n`)
                .value()
                .join("\n"),
            accessories: _(accessories)
                .filter((o) => o.desc)
                .map((o, idx) => `=== ${idx + 1}. ${o.value}==============================\n\n${o.desc}\n`)
                .value()
                .join("\n"),
        });
    }

    const csvFileName = `${moment().format("YYMMDD-HHmmSSS")}_${carName}.txt`;
    console.log("exporting result", csvFileName);
    let stream = fs.createWriteStream(`./carResult/${csvFileName}`);

    stream.once("open", function (fd) {
        stream.write(`url: ${url}\n\n`);
        carListOptions.forEach((car, idx) => {
            stream.write("===========================================================================\n");
            stream.write(`Car ${idx + 1} Key: ${car.carKey}, Name: ${car.carName} Grade: ${car.gradeName}, Price: ${car.price}, Url: ${car.url}\n`);
            stream.write(`\t I. FullEquipment Specs: \n`);
            car.fullEquipmentSpecs.split("\n").forEach((e) => {
                stream.write(`\t\t${e}\n`);
            });
            stream.write(`\t II. Accessories Specs: \n`);
            car.accessories.split("\n").forEach((e) => {
                stream.write(`\t\t${e}\n`);
            });
        });
        stream.end();
        console.log("export done!!!");
    });
};

getListCar();
