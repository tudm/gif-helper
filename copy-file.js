/* eslint-disable no-param-reassign */
const folder = "C:/Users/Admin/Desktop/HUONG QUE/GIAI ĐOẠN 2";
const destFolder = "C:/Users/Admin/Desktop/HUONG QUE/Anh-sai";

const fs = require("fs");
const path = require("path");

const getAllFiles = function (dirPath, arrayOfFiles) {
    let files = fs.readdirSync(dirPath);

    arrayOfFiles = arrayOfFiles || [];

    files.forEach(function (file) {
        if (fs.statSync(`${dirPath}/${file}`).isDirectory()) {
            arrayOfFiles = getAllFiles(`${dirPath}/${file}`, arrayOfFiles);
        } else {
            arrayOfFiles.push(path.join(dirPath, "/", file));
        }
    });

    return arrayOfFiles;
};

const files = fs
    .readFileSync("./copy-file.txt", "utf8")
    .split("\n")
    .filter((x) => x)
    .map((x) => x.trim());

const result = getAllFiles(folder).filter((filePath) => files.find((f) => filePath.indexOf(f) > -1));

result.forEach((f, idx) => {
    const src = f;
    const dest = `${destFolder}/${f.split(/[\\/]/).pop()}`;
    fs.copyFile(src, dest, (err) => {
        if (err) console.log("Error", err);
        else console.log("copied", src, dest);

        if (idx === result.length - 1) {
            console.log("total", result.length);
        }
    });
});
